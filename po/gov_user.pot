# SOME DESCRIPTIVE TITLE.
# Copyright (C) YEAR THE PACKAGE'S COPYRIGHT HOLDER
# This file is distributed under the same license as the PACKAGE package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: 1.2-141-g2924904\n"
"POT-Creation-Date: 2015-09-11 17:06-0000\n"
"PO-Revision-Date: 2015-09-01 20:59-0000\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"Language: \n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=INTEGER; plural=EXPRESSION;\n"

#: plugins/gov_user/lib/ext/search_controller.rb:17
msgid "Institution Catalog"
msgstr ""

#: plugins/gov_user/lib/ext/user.rb:19
msgid "Email must be different from secondary email."
msgstr ""

#: plugins/gov_user/lib/ext/user.rb:40
msgid "E-mail or secondary e-mail already taken."
msgstr ""

#: plugins/gov_user/lib/ext/user.rb:50
msgid "Invalid secondary email format."
msgstr ""

#: plugins/gov_user/lib/ext/organization_rating.rb:16
msgid "not found"
msgstr ""

#: plugins/gov_user/lib/gov_user_plugin.rb:17
msgid "Add features related to Brazilian government."
msgstr ""

#: plugins/gov_user/lib/gov_user_plugin.rb:132
#: plugins/gov_user/lib/gov_user_plugin.rb:163
msgid "Create Institution"
msgstr ""

#: plugins/gov_user/lib/gov_user_plugin.rb:287
msgid "Institution Info"
msgstr ""

#: plugins/gov_user/lib/gov_user_plugin.rb:312
msgid "Institution"
msgstr ""

#: plugins/gov_user/lib/institutions_block.rb:4
#: plugins/gov_user/views/person_editor_extras.html.erb:11
msgid "Institutions"
msgstr ""

#: plugins/gov_user/lib/institutions_block.rb:12
msgid "{#} institution"
msgid_plural "{#} institutions"
msgstr[0] ""
msgstr[1] ""

#: plugins/gov_user/lib/institutions_block.rb:16
msgid "This block displays the institutions in which the user is a member."
msgstr ""

#: plugins/gov_user/lib/institutions_block.rb:24
#: plugins/gov_user/lib/institutions_block.rb:30
msgid "institutions|View all"
msgstr ""

#: plugins/gov_user/lib/institution.rb:47
msgid "invalid, only public and private institutions are allowed."
msgstr ""

#: plugins/gov_user/lib/institution.rb:59
#: plugins/gov_user/lib/institution.rb:73
#: plugins/gov_user/lib/institution.rb:86
msgid "can't be blank"
msgstr ""

#: plugins/gov_user/lib/institution.rb:103
msgid "invalid format"
msgstr ""

#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:18
#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:43
#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:83
msgid "Select a Governmental Sphere"
msgstr ""

#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:19
#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:44
#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:90
msgid "Select a Governmental Power"
msgstr ""

#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:20
#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:45
#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:96
msgid "Select a Juridical Nature"
msgstr ""

#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:21
#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:46
msgid "Select a state"
msgstr ""

#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:168
#: plugins/gov_user/controllers/gov_user_plugin_myprofile_controller.rb:26
msgid "Could not find Governmental Power or Governmental Sphere"
msgstr ""

#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:214
msgid "Institution successful created!"
msgstr ""

#: plugins/gov_user/controllers/gov_user_plugin_controller.rb:219
msgid "Institution could not be created!"
msgstr ""

#: plugins/gov_user/test/unit/gov_user_person_test.rb:50
#: plugins/gov_user/test/unit/gov_user_person_test.rb:56
msgid "Name Should begin with a capital letter and no special characters"
msgstr ""

#: plugins/gov_user/views/search/institutions.html.erb:3
msgid "Type words about the %s you're looking for"
msgstr ""

#: plugins/gov_user/views/ratings_extra_field.html.erb:2
msgid "Organization name or Enterprise name"
msgstr ""

#: plugins/gov_user/views/ratings_extra_field.html.erb:6
#: plugins/gov_user/views/person_editor_extras.html.erb:21
msgid "No institution found"
msgstr ""

#: plugins/gov_user/views/incomplete_registration.html.erb:3
msgid "Complete Profile"
msgstr ""

#: plugins/gov_user/views/incomplete_registration.html.erb:6
msgid "Complete your profile"
msgstr ""

#: plugins/gov_user/views/incomplete_registration.html.erb:7
msgid "Hide"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:1
msgid "Edit Institution"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:5
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:5
msgid ""
"Note that the creation of communities in this environment is restricted. "
"Your request to create this new community will be sent to %{environment} "
"administrators and will be approved or rejected according to their methods "
"and criteria."
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:11
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:11
msgid "\"Can`t create new Institution: #{flash[:errors].length} errors\""
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:24
msgid "All fields with (*) are mandatory"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:31
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:37
msgid "Public Institution"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:36
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:33
msgid "Private Institution"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:43
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:44
msgid "Institution name already exists"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:47
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:48
msgid "Corporate Name"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:52
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:53
msgid "Country"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:56
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:57
msgid "State"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:66
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:66
msgid "CNPJ"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:73
#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:75
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:72
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:74
msgid "Acronym"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:74
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:73
msgid "Fantasy name"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:82
#: plugins/gov_user/views/profile/_institution_tab.html.erb:17
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:81
msgid "Governmental Sphere:"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:89
#: plugins/gov_user/views/profile/_institution_tab.html.erb:16
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:88
msgid "Governmental Power:"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:95
#: plugins/gov_user/views/profile/_institution_tab.html.erb:18
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:94
msgid "Juridical Nature:"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:102
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:101
msgid "SISP?"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:104
#: plugins/gov_user/views/profile/_institution_tab.html.erb:19
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:104
msgid "Yes"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:106
#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:109
#: plugins/gov_user/views/profile/_institution_tab.html.erb:19
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:106
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:108
msgid "No"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin_myprofile/edit_institution.html.erb:114
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:114
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:118
msgid "Save"
msgstr ""

#: plugins/gov_user/views/person_editor_extras.html.erb:2
msgid "Secondary e-mail"
msgstr ""

#: plugins/gov_user/views/person_editor_extras.html.erb:22
msgid "Add new institution"
msgstr ""

#: plugins/gov_user/views/person_editor_extras.html.erb:23
msgid "Create new institution"
msgstr ""

#: plugins/gov_user/views/person_editor_extras.html.erb:39
msgid "Should begin with a capital letter and no special characters"
msgstr ""

#: plugins/gov_user/views/person_editor_extras.html.erb:40
msgid "Email should have the following format: name@host.br"
msgstr ""

#: plugins/gov_user/views/person_editor_extras.html.erb:41
msgid "Site should have a valid format: http://name.hosts"
msgstr ""

#: plugins/gov_user/views/person_editor_extras.html.erb:42
msgid "If you work in a public agency use your government e-Mail"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:3
msgid "Institution Information"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:6
msgid "Type:"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:7
msgid "CNPJ:"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:8
msgid "Last modification:"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:9
msgid "Country:"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:10
msgid "State:"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:11
msgid "City:"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:13
msgid "Fantasy Name:"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:15
msgid "Acronym:"
msgstr ""

#: plugins/gov_user/views/profile/_institution_tab.html.erb:19
msgid "SISP:"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:1
msgid "New Institution"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:16
msgid "\"<b>#{key_name.capitalize}</b> #{value.join()}\""
msgstr ""

#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:115
#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:119
msgid "Cancel"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:121
msgid "Could not send the form data to the server"
msgstr ""

#: plugins/gov_user/views/gov_user_plugin/_institution.html.erb:128
msgid "Creating institution"
msgstr ""
